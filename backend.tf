terraform  {
    backend  "s3" {
        bucket = "my-bucket-sakcan"
        key = "Path/to/my/terraform.tfstate"
        region = "us-east-1"
        encrypt = true
        dynamodb_table = "terraform-lock"

    }
   
}