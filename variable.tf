variable EC2_instance {
    description = "Ec2 instance id"
    default = "ami-0c101f26f147fa7fd"
}

variable EC2_instance_type {
    description = "Ec2 instance type"
    default = "t2.micro"
}

variable "vpc_cidr" {
    default = "10.0.0.0/18"
}

variable "subnet_cidr" {
    default = "10.0.0.0/24"
}